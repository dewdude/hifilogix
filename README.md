![HiFiLOGIX](images/logo.jpg "HiFiLOGIX")

Arduino Control for Your Vintage Stereo

# HiFiLOGIX Is Still Active!

 HiFiLOGIX has progressed to the PCB engineering phase. I am currently working on a PCB design as well as some 
minor changes to how it works due to the inclusion of the ATMega on the board. During this phase there won't be
any real updates to the software nor will I publish the actual schematic used to create the PCB until I feel it's
ready. The major change is the ATMega328 is now integrated to the board and there is some thought as to dropping
the analog matrix for discreet digital switches.

Please do not be discouraged by an apparent lack of activity. It's kind of like a space mission; the exciting part
of watching the rocket launch is over...but the mission is far from complete. I hope to have PCBs in my hand by summer.

# History

```
28-Dec-2019: Initial Version. Just reads five switches and triggers an output.
29-Dec-2019: Version 0.1 - Added a display. Cleaned up code. Made a repository.
29-Dec-2019: Version 0.1.1 - Added preliminary IR remote support
30-Dec-2019: Version 0.1.2 - Controls ShiftReset and OutputEnable on 595 to prevent random start-up state.
30-Dec-2019: Version 0.1.3 - Removed ShiftReset function. Replaced with reset circuit onboard.
01-Jan-2020: Added license, media, and proper README.
04-Jan-2020: Version 0.1.4 - Changed project name. Removed 1602 display code. Added 128x32 OLED code
05-Jan-2020: Version 0.1.5 - Fixed stupid mistake in hardware. Flipped logic of srEnable for new circuit.
05-Jan-2020: Version 0.1.6 - Lots of code clean-up, condensing, rewriting. 
06-Jan-2020: Version 0.1.7 - More code clean-up, practical rewrite of main loop. Bitmapped the logo.
09-Jan-2020: Version 0.1.8 - Code consolidation. Additional beta version with EEPROM support. Added utilities.
10-Jan-2020: Version 0.1.9 - Moved input name table to program space.
11-Jan-2020: Version 0.2.0 - EEPROM code now out of beta. Writes with hardware interrupt now.
08-Feb-2020: Long-delayed Update
````


# About

HiFiLOGIX is an Arduino Sketch to add modern logic control to vintage analog stereo systems. It is being
built and developed around my own personal vintage amplifier; the Denon PMA-770. It is written by a guy
who knows a lot about analog audio circuits and virtually nothing about Arduino.

# About the PMA-770 & Why This Exists

The Denon PMA-770 is a high-end integrated amplfier from the early 80's capable of the usual 100 watts into
8 ohms; and somewhere in the neighborhood of 120 to 140 in to a 4 ohm load. It's a DC coupled amplifier of
"Direct-A" topology. It's got 3 pairs of output transistors, no capacitors in the audio path, and it sounds
outstanding.

But it's also got this seriously stupid way of working the input switching that has not only caused failure
of most of the inputs; but is silly enough I don't want to bother trying to replace the switches to keep the
original method in tact. In order for the audio to make it in to the amplifier, it relies on connecting through
the "normally closed" contact on the switch "in front" of it. Well the "normally closed" contact has decided it
no longer wants to close and the entirely sealed design has prevented cleaning agents to make it in.

To add to the stupidity; this unit features a simple segemented LCD display to display the name of the input.
This was a probably a pretty big ordeal in an era where even a single LED lighting up over the name was an 
entirely new idea. This display is non-standard and relies on even more contacts in each input switch to throw
pins to AC. Disabling the input switches would disable this display; but the 128x32 OLED looks like it will fit
in the phsical space the existing one does.

Taking that in to account, I decided to build a relay board driven by an Arduino that would also drive the 
display. It also lets me do crazy things like add input selection by remote control...something you weren't
getting back in 1981. Additional modifications like motorized pots for volume could further extend the 
remote control function. 

# Hardware

The basic idea is parallel the actuator contacts of DPDT (or DPST) relays and flip them on one at a time (two
for phono).

[![Schematic of hardware](/images/schematicsmall.png)](/images/schematic.png)
Click for full resolution.

Data is shifted in to a 74HC595 and it's parallel outputs are used to control a ULN2803(or 2003) darlington array to
in turn drive a relay. Every effort has been taken to ensure only one relay will be fired at a time to prevent damage
to other audio components. The output of the 74HC595 is controlled by the blanking circuit that also controls the 2003.
In version 0.1.5/hardware 1.4 this was slightly modified. A simple transistor logic inverter now drives both the 74HC595
enable pin as well as the ULN2803/2003's ground line. This was done because the 2003 I'm using likes to boot up with 
every output on even when pulling the lines down. Calculating the relays to draw about 40mA each; trying to sink this
through the arduino using the existing logic low for the 595 was probably a bad idea. So the logic in the software was
flipped and the transistor now converts the logic-high to "logic low" and is capable of sinking something like 800mA if
I read the datasheet right. More than enough for the 80mA I predict I'll need from the thing. While I wanted to keep parts
down (and complexity) low; keeping a low number of pins to the micro-controller is the priority and the transistor and two 
resistors not only aren't expensive, but meet that goal.

The 2803/2003 arrays include built in snubber diodes for inductive kick-back protection. If you should deviate from this
schematic and use discreet diodes, you must add these diodes to your circuit or you will damage the transistors, shift
register, and/or Adruino.

The switch matrix consists of a simple resistor based analog method that merely throws different amounts of voltage thru
resistors. While this allows us to read all of our switches using just one line, it can cause problems should you want to
trigger two buttons. On the PMA-770 this is fine as the original bank of switches are mechanically linked to allow only 
one to be active at a time. It is possible to read dual inputs as long as the parallel resistance doesn't cause a value
close to any indvidual switch. I have included a small utility in the /utilities folder that will display the current
ADC integer value to the LCD display. It can be used for just figuring out which resistors to use, or to check behavior
of paralelled resistors (two button press scenario). Using one line for the switch matrix is not set in stone and may 
change depending on needs and availability of pins going forward.

The Phono-1 and Phono-2 inputs are the only time the unit will ever fire two relays. This is because on the original 
unit itself; both of these inputs route the phono-stage preamp to the main amplifier while routing the cartridge input to
the phono-stage. 

An 1838 IR sensor is used for remote control function. An additional utility may be written to help with this going forward
but it's possible I may wind up writing a routine for "self-programming". 

As of Version 0.1.4 we now support the SSD1306 based 128x32 OLED Display.

Despite the incorrect relays; I decided to build a version on some protoboard as a throwback to the old microcontroller demo
units. It is literally just a board that contains the same relays driving LEDs as the breadboard version; except everything 
is soldered together and the ATMega has been liberated from the Uno board and integrated. This ATMega also runs on the internal
8MHz clock.

# EEPROM Usage

Version 0.2.0 brought the EEPROM code out of beta. It is now a default feature of the main version. 

THE SKETCH DOESN'T CARE WHAT DATA YOU HAVE AT 1020 - 1023 OR IF YOU HAVE THE CIRCUIT TO PROPERLY USE IT. IT WILL WRITE OVER
IT THE FIRST TIME YOU RUN IT. 

YOU MUST CHANGE THE VALUES IF YOUR EEPROM IS SMALLER OR WANT TO USE DIFFERENT ADDRESSES.

The sketch currently uses 4 bytes of EEPROM space to store the input last selected at power off. One byte is used to store the
byte sent to the shift register, one stores the x offset for the display, the other stores the index to the array that contains
the name; the exact same information used for arguments to DontCrossTheStreams(). This information is read during boot.

In order to ensure there's valid data; the fourth byte is used to indicate if the initialization routine has run. If the last
byte contains "T", then we assume the data we want is valid. If T is not found; then the function writes the default data for
the AUX input and writes the initalization flag. 

Data is written to the EEPROM by way of a hardware interrupt. The basic idea is to throw a lot of capacitance on to the Arduino
in a way that it's isolated to the Arduino and montior "the other side" of the 5V rail. When the 5V line drops as a result of
turning off the amplifier; the capacitor should power the Arduino long enough to detect the falling state of pin 2 and write the
data to the EEPROM...if necessary. 

If you leave pin 2 floating it seems to do nothing. If you connect it to ground it does nothing. If it drops out at the same time
as the Arduino, it does nothing. If you do nothing with this pin the thing behaves just like before; with the exception that the
default never changes.  

# Software

This sketch requires the following libaries:
	- IRRemote (https://github.com/z3t0/Arduino-IRremote)
	- U8G2 Display Driver (https://github.com/olikraus/u8g2)

Please install them via Library Manager or however you install libaries. Check indvidual project pages for LICENSE info.


# Media

[![Protoboard Version](/images/proto2.jpg "Protoboard Version")*Protoboard Version*][/images/proto2full.jpg]
![Picture of prototype rig](/images/prototype.jpg "Prototype")*Prototype & Development Build*

More recent prototype build.

I'm horrible at making videos in general...but I still made an attempt to show things off. Youtube embedding is apparently 
broken and I don't feel like putting a gig worth of video in the repository, so you'll just have to use links.

[First Video: General Overview](https://www.youtube.com/watch?v=I_sLnnsZ7Qg)

[Now I've Got A Remote Control](https://www.youtube.com/watch?v=I30OaMii4l4)

[OLED, Relay Control, and HiFiLOGIX] (https://www.youtube.com/watch?v=vn3nhpWjckw)

[EEPROM Beta Demo] (https://www.youtube.com/watch?v=cMGZij3sTnE)

# License

Copyright 2020 Jay Moore 

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this 
list of conditions and the following disclaimer. 

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
and the following disclaimer in the documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.


