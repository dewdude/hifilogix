HiFiLOGIX Utilities

The following sketches have been created to aid you should you decide to build
and use HiFiLOGIX for your own system. 

# Analog Switch Matrix Helper

This literally just reads the value returned by the ADC on A0 and displays it to
the serial console. 

It is recommended that you take these measurements with a pull-down resistor on A0. 

Upload the sketch, open serial monitor. You should either see a bunch of 0's if you
have a pull-down resistor connected; or some wildly changing numbers if you don't. 
Stick a resistor between A0 and 5V. The ADC in the ATMega will spit out a number 
between 0 and 1023 and display it on the console. I really can't stress enough how
much better this works if you have a pull-down on A0. 

In theory you could calculate all this out by figuring out what voltage the A0 pin
would see in the voltage-divider you're creating; then figuring out what it's 5-bit
sampled value would be. But that's a pain in the ass.