/* 
 *  Analog Switch Matrix Helper
 *  
 *  This sketch basically spits out the integer value of the A0 line
 *  to the serial console. Just plop a resistor between 5V and A0.
 *  The code in HiFiLOGIX is set up to use a range to account for
 *  drift and other variables.
 *  
 */

char data[100];

void setup() {
Serial.begin(9600)
}

void loop() {
  int ain = analogRead(A0);
  sprintf(data,"%4d",ain);
  Serial.println(data);
  delay(250);
}
