#define hifilogix_width 98
#define hifilogix_height 24
static const unsigned char hifilogix_bits[] U8X8_PROGMEM = {
   0x01, 0x10, 0xe0, 0xff, 0x40, 0x00, 0xc0, 0xff, 0xe0, 0x3f, 0x88, 0x00,
   0x02, 0x01, 0x10, 0x11, 0x00, 0x48, 0x00, 0x20, 0x00, 0x11, 0x40, 0x08,
   0x01, 0x01, 0x01, 0x10, 0x08, 0x00, 0x40, 0x00, 0x10, 0x00, 0x09, 0x80,
   0x08, 0x82, 0x00, 0x01, 0x10, 0x08, 0x00, 0x40, 0x00, 0x10, 0x00, 0x09,
   0x00, 0x08, 0x44, 0x00, 0x01, 0x10, 0x09, 0x00, 0x48, 0x00, 0x10, 0x00,
   0x09, 0x00, 0x08, 0x28, 0x00, 0xf9, 0x1f, 0xc9, 0x7f, 0x48, 0x00, 0x10,
   0x00, 0x09, 0xfe, 0x08, 0x10, 0x00, 0x01, 0x10, 0x09, 0x00, 0x48, 0x00,
   0x10, 0x00, 0x09, 0x80, 0x08, 0x28, 0x00, 0x01, 0x10, 0x09, 0x00, 0x48,
   0x00, 0x10, 0x00, 0x09, 0x80, 0x08, 0x44, 0x00, 0x01, 0x10, 0x09, 0x00,
   0x48, 0x00, 0x10, 0x00, 0x09, 0x80, 0x08, 0x82, 0x00, 0x01, 0x10, 0x09,
   0x00, 0x48, 0x00, 0x10, 0x00, 0x11, 0x40, 0x08, 0x01, 0x01, 0x01, 0x10,
   0x09, 0x00, 0x88, 0xff, 0xe3, 0xff, 0xe0, 0x3f, 0x88, 0x00, 0x02, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x3f, 0x7f, 0x43, 0x3e, 0x43, 0x80, 0xbf, 0xa0, 0x3f, 0xc0,
   0xef, 0xf7, 0x03, 0x41, 0x01, 0x43, 0x41, 0x43, 0x80, 0xa0, 0xb1, 0x20,
   0x00, 0x08, 0x14, 0x02, 0x41, 0x01, 0x45, 0x41, 0x45, 0x80, 0xa0, 0xaa,
   0x20, 0x00, 0x08, 0x14, 0x02, 0x41, 0x01, 0x45, 0x41, 0x45, 0x80, 0xa0,
   0xa4, 0x20, 0x00, 0x08, 0x14, 0x02, 0x41, 0x1f, 0x49, 0x41, 0x49, 0x80,
   0xbf, 0xa0, 0x20, 0x3f, 0x08, 0x14, 0x02, 0x41, 0x01, 0x49, 0x41, 0x49,
   0x80, 0x80, 0xa0, 0x3f, 0x00, 0x08, 0x14, 0x02, 0x41, 0x01, 0x51, 0x41,
   0x51, 0x80, 0x80, 0xa0, 0x20, 0x00, 0x08, 0x14, 0x02, 0x41, 0x01, 0x51,
   0x41, 0x51, 0x80, 0x80, 0xa0, 0x20, 0x00, 0x08, 0x14, 0x02, 0x41, 0x01,
   0x61, 0x41, 0x61, 0x80, 0x80, 0xa0, 0x20, 0x00, 0x08, 0x14, 0x02, 0x3f,
   0x7f, 0x61, 0x3e, 0x61, 0x80, 0x80, 0xa0, 0x20, 0x00, 0x08, 0xf4, 0x03 };
